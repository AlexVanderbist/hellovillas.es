import 'babel-polyfill';
import viewport from 'viewport-utility';
import axios from 'axios';

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

viewport.init({
    root: $('.js-viewport'),
    classPrefix: 'is-',
    config: {
        start: 70,
        end: 70,
        scrollOffset: 100,
    },
});

/////////////

import objectFitImages from 'object-fit-images';

function updateSrcSetSize() {
    $('.js-srcset-size').each(function () {
        $(this).attr('sizes', Math.ceil($(this).width()) + 'px');
    });
}

let resizeTimer;

$(document).ready(() => {
    updateSrcSetSize();
    objectFitImages('.js-object-fit');
});

$(window).resize(() => {

    clearTimeout(resizeTimer);

    resizeTimer = setTimeout(() =>{
        updateSrcSetSize();
        objectFitImages('.js-object-fit');

    }, 250);
});

/////////////

import Vue from 'vue';

import ImageGallery from './components/ImageGallery';
import HouseMap from './components/HouseMap';
import CollapsiblePanel from './components/CollapsiblePanel';

Vue.component('image-gallery', ImageGallery);
Vue.component('house-map', HouseMap);
Vue.component('collapsible-panel', CollapsiblePanel);

new Vue({
    el: '#app',
});

////////////////

document.addEventListener('DOMContentLoaded', function () {

    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(function ($el) {
            $el.addEventListener('click', function () {

                // Get the target from the "data-target" attribute
                var target = $el.dataset.target;
                var $target = document.getElementById(target);

                // Toggle the class on both the "navbar-burger" and the "navbar-menu"
                $el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});


@component('front._layouts.main', [
    'meta' => $article->meta(),
    'subMenu' => Menu::articleSiblings($article),
])

@slot('mainImages')
    @if($cover = $article->getFirstMedia('images'))
        @include('front.houses._partials.banner', ['cover' => $cover, 'size' => 'is-medium'])
    @endif
@endslot

<section class="section is-small">
    <div class="container">
        <div class="columns">
            <div class="column is-two-thirds content">
                {!! $article->text !!}

                <p>
                    <a href="mailto:{{ __('company.email') }}">{{ __('company.email') }}</a> <br>
                    tel. <a href="tel:{{ __('company.telephone') }}">{{ __('company.telephone') }}</a> <br>
                </p>

                <p>
                    {{ __('company.name') }} <br>
                    {{ __('company.address') }} <br>
                    {{ __('company.postal') }} {{ __('company.city') }} <br>
                    {{ __('company.country') }} <br>
                </p>
            </div>
            <div class="column is-one-third">
                @include('front.contact._partials.form')
            </div>
        </div>
    </div>
</section>
@endcomponent


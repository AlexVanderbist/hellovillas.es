<div class="box" id="contact-form">
    @if(session('flash_notification'))
        <div class="notification is-primary">
            @include('flash::message')
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", () => document.getElementById('contact-form').scrollIntoView({ block: 'center'}));
        </script>
    @endif

    {{ html()->form('POST', route('contact.handle'))->open() }}

    {!! Honeypot::make('first_name', 'email_address') !!}

    {{ html()->formGroup()->required()->text('name', 'form.name') }}
    {{ html()->formGroup()->required()->text('telephone', 'form.telephone') }}
    {{ html()->formGroup()->required()->email('email', 'form.email') }}

    {{--{{ html()->formGroup()->text('address', 'form.address') }}--}}

    {{--<div class="columns">--}}
        {{--<div class="column is-3">--}}
            {{--{{ html()->formGroup()->text('postal', 'form.postal') }}--}}
        {{--</div>--}}
        {{--<div class="column is-9">--}}
            {{--{{ html()->formGroup()->text('city', 'form.city') }}--}}
        {{--</div>--}}
    {{--</div>--}}

    {{ html()->formGroup()->required()->textarea('remarks', 'form.remarks', 4) }}

    {{ html()->button(__('form.submit'), 'submit')->class('button is-primary') }}

    {{ html()->form()->close() }}

    <p class="help is-info">
        * {{ __('form.fieldsAreRequired') }}
    </p>
</div>

@component('front._layouts.main', [
    'meta' => $house->meta(),
])
    @slot('mainImages')
        @if($cover = $house->getFirstMedia('banner'))
            @include('front.houses._partials.banner', ['cover' => $cover])
        @endif
    @endslot

    <section class="section">
        <div class="container">
            @include('front.houses._partials.header')
            <div class="columns">
                <div class="column is-two-thirds">
                    <collapsible-panel
                        class="content"
                        open-text="{{ __('house.readMore') }}"
                        close-text="{{ __('house.readLess') }}"
                    >
                        {!! $house->text !!}
                    </collapsible-panel>
                </div>
                <div class="column is-one-third">
                    @include('front.houses._partials.sidebar')
                </div>
            </div>
        </div>
    </section>

    @include('front.houses._partials.images')

    <section class="hero is-purple is-dark">
        <div class="hero-body">
            <div class="container">
                <div class="columns">
                    <div class="column is-two-thirds">
                        <h2 class="title">{{ __('house.locationTitle') }}</h2>
                        <div class="content">
                            {!! $house->location_text !!}
                        </div>
                    </div>
                    <div class="column is-one-thirds">
                        <house-map
                            lat="{{ $house->location_lat }}"
                            lng="{{ $house->location_lng }}"
                            :zoom="{{ $house->location_zoom }}"
                            title="{{ $house->name }}"
                        ></house-map>
                        <p class="content m-t-md">
                            <a href="{{ $house->googleMapsUrl }}" target="_blank">
                                {{ __('house.openInGoogleMaps') }}
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endcomponent


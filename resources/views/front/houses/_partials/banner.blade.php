@php($size = $size ?? 'is-large')
<section class="hero is-dark {{ $size }}" style="background-image:url('{{ $cover->getUrl('2400w') }}')">
    <div class="hero-body">
    </div>
</section>

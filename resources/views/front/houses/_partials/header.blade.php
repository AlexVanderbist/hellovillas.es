<h1 class="title has-line is-spaced">{{ $house->name }}</h1>
<h2 class="subtitle">
    <div class="icon"><i class="fa fa-map-marker"></i></div>
    {{ $house->location_name }}, {{ __('house.spain') }}
    @if(! $house->is_available)
        <span class="tag is-danger is-light is-uppercase m-l-sm">Currently unavailable</span>
    @endif
    @if($house->for_sale && $house->price)
        <span class="tag is-medium is-danger is-light is-uppercase m-l-sm">&euro; {{ number_format($house->price, 0, ',', ' ') }}</span>
    @endif
</h2>

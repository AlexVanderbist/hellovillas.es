<div class="box sticky">
    <ul class="list">
        @if($house->for_rent)
            <li>
                <a href="#">
                    <div class="level">
                        <div class="level-left">
                            @foreach(range(0, 3) as $star)
                            <span class="icon is-small has-text-warning">
                                <i class="fa fa-star"></i>
                            </span>
                            @endforeach
                        </div>
                        <div class="level-right">
                            4 reviews
                        </div>
                    </div>
                </a>
            </li>
        @endif
        @if($house->interior_area)
            <li>
                <div class="icon">
                    <i class="fa fa-house"></i>
                </div>
                {{ $house->interior_area }}&#13217 interior
            </li>
        @endif
        @if($house->exterior_area)
            <li>
                <div class="icon">
                    <i class="fa fa-location-dot"></i>
                </div>
                {{ $house->exterior_area }}&#13217 land
            </li>
        @endif
        <li>
            <div class="icon">
                <i class="fa fa-bed"></i>
            </div>
            {{ $house->bedrooms }} {{ str_plural('bedroom', $house->bedrooms) }}
        </li>
        @if($house->single_beds)
            <li>
                <div class="icon">
                    <i class="fa fa-bed"></i>
                </div>
                {{ $house->single_beds }} single {{ str_plural('bed', $house->single_beds) }}
            </li>
        @endif
        @if($house->double_beds)
            <li>
                <div class="icon">
                    <i class="fa fa-bed"></i>
                </div>
                {{ $house->double_beds }} double {{ str_plural('bed', $house->double_beds) }}
            </li>
        @endif
        @if($house->bathrooms)
            <li>
                <div class="icon">
                    <i class="fa fa-bath"></i>
                </div>
                {{ $house->bathrooms }} {{ str_plural('bathroom', $house->bathrooms) }}
            </li>
        @endif
        <li style="display: grid; grid-template-columns: repeat(6, 1fr); padding-left: 0">
            @foreach($house->amenities ?? [] as $amenity)
                <div class="p-sm has-text-centered">
                    <div class="icon" title="{{ $amenity }}">
                        <i class="fa fa-{{amenityIcon($amenity)}}"></i>
                    </div>
                </div>
            @endforeach
        </li>
    </ul>

    @if($house->for_rent && $house->hasExternalBooking())
        <a href="{{ $house->booking_url }}" class="button is-primary is-fullwidth">
            <i class="fa fa-external-link"></i>
            &nbsp; Book now
        </a>
    @endif

    @if($house->for_sale)
        <a href="{{ action('\App\Http\Controllers\Front\ContactController@index') }}" class="button is-primary is-fullwidth">
            <i class="fa fa-envelope"></i>
            &nbsp; Contact us
        </a>
    @endif
</div>

<footer class="footer is-dark is-medium">
    <div class="is-overlay" style="background-image:url({{ $backgroundImage->getUrl('2400w') }})"></div>
    <div class="is-overlay color-overlay"></div>
    <div class="container">
        <div class="columns is-centered">
            <div class="column is-8">
                <div class="columns">
                    <div class="column is-one-quarter">
                        <ul>
                            <li><a href="{{ article('home')->url }}" class="has-text-weight-bold">HelloVillas</a></li>
                            <li><a href="{{ article('home')->url }}">Home</a></li>
{{--                            <li><a><i class="fa fa-2x fa-instagram"></i></a></li>--}}
                        </ul>
                    </div>
                    <div class="column is-one-quarter">
                        <ul>
                            <li><a href="{{ article(\App\Models\Enums\SpecialArticle::PROPERTIES_TO_RENT)->url }}" class="has-text-weight-bold">Properties to rent</a></li>
                            @foreach($houses as $house)
                                <li><a href="{{ $house->url }}">{{ $house->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="column is-one-quarter">
                        <ul>
                            <li><a href="{{ article('service')->url }}" class="has-text-weight-bold">Our services</a></li>
                            @foreach($services as $service)
                                <li><a href="{{ $service->url }}">{{ $service->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="column is-one-quarter">
                        <ul>
                            <li><a href="#" class="has-text-weight-bold">Contact</a></li>
                            <li><a href="mailto:{{ __('company.email') }}">{{ __('company.email') }}</a></li>
                            <li>{{ __('company.telephone') }}</li>
                        </ul>
                        {!! schema()->company() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="has-text-centered has-text-weight-bold">
            <div class="columns is-centered is-mobile">
                <div class="column is-narrow">
                    <figure class="image m-30 is-48x48">
                        @svg('logo-outlines-fat')
                    </figure>
                </div>
            </div>

            Copyright &copy; HelloVillas.es {{ date('Y') }}
        </div>
    </div>
</footer>

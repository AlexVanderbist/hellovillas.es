@php
  /** @var \App\Models\Article $article */

    $articleHasActiveHouse=$article->houses->contains(function (\App\Models\House $house) {
        return Spatie\Menu\ActiveUrlChecker::check($house->url, request()->url());
    });

    $active = $articleHasActiveHouse || Spatie\Menu\ActiveUrlChecker::check($article->url, request()->url());
@endphp
<div class="navbar-item has-dropdown is-hoverable {{ $active ? 'active' : '' }}">
    <a class="navbar-link">
        {{ $article->name }}
    </a>

    <div class="navbar-dropdown navbar-dropdown--house">
        @foreach($article->children()->online()->get() as $child)
            @php($active = Spatie\Menu\ActiveUrlChecker::check($child->url, request()->url()))
            <a href="{{ $child->url }}" class="navbar-item {{ $active ? 'active' : '' }}">
                {{ $child->name }}
            </a>
        @endforeach

        @foreach($article->houses()->online()->get() as $house)
            @php($active = Spatie\Menu\ActiveUrlChecker::check($house->url, request()->url()))
            <a href="{{ $house->url }}" class="navbar-item {{ $active ? 'active' : '' }}">
                {{ $house->name }}
            </a>
        @endforeach
    </div>
</div>

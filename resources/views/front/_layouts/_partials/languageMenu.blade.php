<div class="navbar-item has-dropdown is-hoverable">
    <a class="navbar-link navbar-link--language">
        {{ locale() }}
    </a>

    <div class="navbar-dropdown navbar-dropdown--language">
        @foreach(locales() as $language)
            <a href="/{{ $language }}" class="navbar-item {{ locale() === $language ? 'active' : '' }}">
                {{ $language }}
            </a>
        @endforeach
    </div>
</div>

@php($active = Spatie\Menu\ActiveUrlChecker::check(route('houses.show'), request()->url()))
<div class="navbar-item has-dropdown is-hoverable {{ $active ? 'active' : '' }}">
    <a class="navbar-link">
        {{ __('home.ourHouses') }}
    </a>

    <div class="navbar-dropdown navbar-dropdown--house">
        @foreach($houses as $house)
            @php($active = Spatie\Menu\ActiveUrlChecker::check(route('houses.show', $house->slug), request()->url()))
            <a href="{{ $house->url }}" class="navbar-item {{ $active ? 'active' : '' }}">
                {{ $house->name }}
            </a>
        @endforeach
    </div>
</div>

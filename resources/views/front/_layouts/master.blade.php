<!DOCTYPE html>
<html lang="{{ locale() }}" class="js-viewport | html--stretched">
<head>
    @include('front._layouts._partials.head.meta')

    <script src="https://kit.fontawesome.com/2a1036d4b2.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/front.css') }}">
    <script src="{{ mix('js/front.head.js') }}"></script>

    @include('front._layouts._partials.head.seo')
    @include('front._layouts._partials.head.hreflang')
    @include('front._layouts._partials.head.favicons')
    @include('front._layouts._partials.head.bugsnag')
</head>
<body>
    @include('googletagmanager::script')
    @include('front._layouts._partials.deprecatedBrowser')

    @include('front._layouts._partials.header')
    @include('front._layouts._partials.flashMessage')

    {{ $slot }}

    @include('cookieConsent::index')
    @include('front._layouts._partials.footer')

    <script src="{{ mix('js/front.app.js') }}"></script>
</body>
</html>

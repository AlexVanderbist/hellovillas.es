@component('front._layouts.main', [
    'meta' => $article->meta(),
])
    @slot('mainImages')
        @if($cover = $article->getFirstMedia('images'))
            @include('front.home._partials.cover', ['cover' => $cover])
        @endif
    @endslot

    <section class="section p-t-xl p-b-xl">
        <div class="container">
            <h1 class="title has-line">{{ $article->name }}</h1>
            <div class="columns">
                <div class="column is-two-thirds">
                    <div class="content">
                        {!! $article->text !!}
                    </div>
                    <a href="{{ route('contact') }}" class="button is-outlined is-info">{{ __('home.textCta') }}</a>
                </div>
                <div class="column is-one-third content">
                    <div class="sticky">
                        {!! __('home.secondaryText') !!}

                        <a href="{{ __('home.secondaryCtaLink') }}" class="cta">
                            {{ __('home.secondaryCta') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('front._partials.twoColumnQuote', ['quote' => $article->getQuote(1)])

    @foreach($houseArticles as $section => $houses)
        @include('front.home._partials.houses', ['title' => $section])
    @endforeach

    @include('front._partials.quote', ['quote' => $article->getQuote(2), 'variant' => 'left'])

    @includeWhen($article->hasContentBlocks(), 'front._partials.contentBlocks', ['blocks' => $article->contentBlocks])

    @include('front._partials.quote', ['quote' => $article->getQuote(3), 'variant' => 'right'])
@endcomponent


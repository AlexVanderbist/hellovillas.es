<section class="section">
    <div class="container">
        <h2 class="title has-text-centered">{{ $title }}</h2>
        <div class="columns is-multiline">
            @foreach($houses as $house)
                <div class="column is-half">
                    <div class="card">
                        <div class="card-image">
                            <figure class="image is-16by9"
                                    style="background-image: url({{ $house->getFirstMediaUrl('banner', '600w') }})">
                            </figure>
                        </div>
                        <div class="card-content">
                            <div class="content">
                                <div class="columns is-gapless">
                                    <div class="column">
                                        <h2 class="title is-4">
                                            {{ $house->name }}
                                            <span>| {{ $house->location_name }}</span>
                                        </h2>
                                    </div>
                                    @if(! $house->is_available)
                                        <div class="column" style="flex-grow: 0">
                                            <span class="tag is-danger is-light is-uppercase m-t-xs">Currently
                                                unavailable
                                            </span>
                                        </div>
                                    @endif
                                </div>
                                <p>
                                    {!!$house->excerpt!!}
                                </p>
                                <div class="controls has-text-right">
                                    <a href="{{ $house->url }}"
                                       class="button is-outlined is-info is-block-touch">Villa details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>

<section class="hero is-dark is-medium" style="background-image:url('{{ $cover->getUrl('2400w') }}')">
    <div class="container">
        <div class="columns m-none p-none">
            <div class="column is-8-tablet is-4-desktop is-offset-two-thirds has-text-right has-background">
                <div class="hero-body">
                    <h1 class="title">
                        {{ __('home.coverTitle') }}
                    </h1>
                    <h2 class="subtitle">
                        {{ __('home.coverSubtitle') }}
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>

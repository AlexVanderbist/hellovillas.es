@component('front._layouts.main', [
    'meta' => $article->meta(),
    'subMenu' => Menu::articleSiblings($article),
])
    @slot('mainImages')
        @if($cover = $article->getFirstMedia('images'))
            @include('front.houses._partials.banner', ['cover' => $cover, 'size' => 'is-medium'])
        @endif
    @endslot

    <div class="section is-small">
        <div class="container">
            <div class="columns is-gapless">
                <div class="content column is-two-thirds">
                    <h1>{{ $article->name }}</h1>
                    {!! $article->text !!}

                    @if($article->show_content_blocks_toc)
                        <ul class="m-l-none" style="list-style: none">
                            @foreach($article->contentBlocks as $block)
                                @php
                                /** @var \App\Models\ContentBlock $block */
                                @endphp
                                <li><a href="#{{ $block->slug }}">{{ $block->name }}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('front._partials.quote', ['quote' => $article->getQuote(1)])

    @includeWhen($article->hasContentBlocks(), 'front._partials.contentBlocks', ['blocks' => $article->contentBlocks])

    @include('front._partials.quote', ['quote' => $article->getQuote(2)])

    @includeWhen($houses, 'front.article._partials.houses', ['houses' => $houses])

    @include('front._partials.quote', ['quote' => $article->getQuote(3)])
@endcomponent

<section class="section">
    <div class="container">
        <div class="columns is-multiline">
            @foreach($houses as $house)
                <div class="column is-one-third">
                    <a href="{{ $house->url }}">
                        <div class="card">
                            <div class="card-image">
                                <figure class="image is-16by9"
                                    style="background-image: url({{ $house->getFirstMediaUrl('banner', '600w') }})">
                                <span
                                    class="tag is-medium is-danger is-light is-uppercase m-l-sm"
                                    style="position: absolute; top: 9px"
                                >
                                    &euro; {{ number_format($house->price, 0, ',', ' ') }}
                                </span>
                                </figure>
                            </div>
                            <div class="card-content p-md">
                                <div class="content">
                                    <div class="columns is-gapless">
                                        <div class="column">
                                            <h2 class="m-b-none title is-4">
                                                {{ $house->name }}
                                                <span>| {{ $house->location_name }}</span>
                                            </h2>
                                        </div>
                                        @if(! $house->is_available)
                                            <div class="column" style="flex-grow: 0">
                                                <span class="tag is-danger is-light is-uppercase m-t-xs">Currently
                                                    unavailable
                                                </span>
                                            </div>
                                        @endif
                                    </div>
    {{--                                <p>--}}
    {{--                                    {{ $house->excerpt }}--}}
    {{--                                </p>--}}
    {{--                                <div class="controls has-text-right">--}}
    {{--                                    <a href="{{ $house->url }}"--}}
    {{--                                        class="button is-outlined is-info is-block-touch">Villa details</a>--}}
    {{--                                </div>--}}
                                </div>
                            </div>
                            <footer class="card-footer">
                                <div class="card-footer-item">
                                    <div class="icon">
                                        <i class="fa fa-house"></i>
                                    </div>
                                    &nbsp; {{ $house->interior_area }}&#13217
                                </div>
                                <div class="card-footer-item">
                                    <div class="icon">
                                        <i class="fa fa-bed"></i>
                                    </div>
                                    &nbsp; {{ $house->bedrooms }}
                                </div>
                                <div class="card-footer-item">
                                    <div class="icon">
                                        <i class="fa fa-bath"></i>
                                    </div>
                                    &nbsp; {{ $house->bathrooms }}
                                </div>
                            </footer>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</section>

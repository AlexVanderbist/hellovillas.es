@component('front._layouts.main', [
    'title' => __('error.title'),
])
    <section class="section is-medium">
        <h1 class="title">{{ __('error.title') }}</h1>

        <p>
            {{ __('error.text404') }}
        </p>
        <p>
            <a class=button href="{{ route('home') }}">
                {{ __('error.toHome') }}
            </a>
        </p>
    </section>
@endcomponent

@php($variant = $variant ?? 'right')

@if($quote['text'])
    <section class="hero is-purple is-dark">
        <div class="hero-body">
            <div class="container">
                <div class="media is-vcentered">
                    @if($variant === 'right')
                        <div class="media-content has-text-right-tablet has-text-left-mobile">
                            <h3 class="is-size-4-tablet is-size-6-mobile m-b-md">
                                {{ $quote['text'] }}
                            </h3>
                            <p class="subtitle has-text-weight-bold">
                                {{ $quote['author'] }}
                            </p>
                        </div>
                        @if($quote['image'])
                            <div class="is-hidden-mobile media-right">
                                <figure class="image is-128x128 is-circle has-light-border">
                                    <img src="{{ $quote['image']->getUrl('thumb_square') }}" alt="{{ $quote['image']->name }}">
                                </figure>
                            </div>
                        @endif
                    @endif

                    @if($variant === 'left')
                        @if($quote['image'])
                            <div class="is-hidden-mobile media-left">
                                <figure class="image is-128x128 is-circle has-light-border">
                                    <img src="{{ $quote['image']->getUrl('thumb_square') }}" alt="{{ $quote['image']->name }}">
                                </figure>
                            </div>
                        @endif
                        <div class="media-content has-text-left">
                            <h3 class="is-size-4-tablet is-size-6-mobile m-b-md">
                                {{ $quote['text'] }}
                            </h3>
                            <p class="subtitle has-text-weight-bold">
                                {{ $quote['author'] }}
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endif

@if($quote['text'])
    <section class="hero is-purple is-dark">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-vcentered">
                    <div class="column is-half">
                        <h3 class="is-size-4-tablet is-size-6-mobile m-b-md">
                            {{ $quote['text'] }}
                        </h3>
                        <p class="subtitle has-text-weight-bold">
                            {{ $quote['author'] }}
                        </p>
                    </div>
                    @if($quote['image'])
                        <div class="column is-half media-right">
                            <figure class="image has-light-border">
                                <img src="{{ $quote['image']->getUrl() }}" alt="{{ $quote['image']->name }}">
                            </figure>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endif

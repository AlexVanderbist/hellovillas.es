@foreach($blocks as $block)
    <section class="section">
        <div class="container">
            <a class="anchor" id="{{ $block->slug }}"></a>
            @include("front._partials.contentBlocks.{$block->type}")
        </div>
    </section>
@endforeach

<div class="columns">
    <div class="column is-7 content">
        <h3>{{ $block->name }}</h3>
        {!! $block->text !!}
    </div>

    <div class="column is-offset-1 is-4">
        @include('front.contact._partials.form')
    </div>
</div>

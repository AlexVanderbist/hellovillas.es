@if($block->name)
    <div class="block">
        <h3 class="is-size-4">{{ $block->name }}</h3>
    </div>
@endif
<div class="columns">
    <div class="column is-8 content">
        {!! $block->text !!}
    </div>
    <div class="column is-4">
        <img src="{{ $block->getFirstMediaUrl('images', 'square') }}" alt="{{ $block->name }}">
    </div>
</div>

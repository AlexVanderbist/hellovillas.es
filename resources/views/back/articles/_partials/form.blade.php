<div data-tabs class="tabs">
    <nav class="tabs__menu">
        <ul>
            <li><a href="#content" class="js-tabs-nav"><i class="fa fa-edit"></i> Content</a></li>
            <li><a href="#quotes" class="js-tabs-nav"><i class="fa fa-quote-left"></i> Quotes</a></li>
            <li><a href="#settings" class="js-tabs-nav"><i class="fa fa-cog"></i> Settings</a></li>
            <li><a href="#blocks" class="js-tabs-nav"><i class="fa fa-cube"></i> Blocks</a></li>
            <li><a href="#seo" class="js-tabs-nav"><i class="fa fa-code"></i> SEO</a></li>
        </ul>
    </nav>
    <div id="content">
        {{ html()->translations(function () {
            return [
                html()->formGroup()->required()->text('name', 'Name'),
                html()->formGroup()->required()->redactor('text', 'Text'),
            ];
        }) }}

        {{ html()->formGroup()->media('images', 'images', 'Images') }}
        {{ html()->formGroup()->media('downloads', 'downloads', 'Downloads') }}
    </div>
    <div id="quotes">
        @foreach(range(1, 3) as $quote)
            <h2 class="mb-4">Quote {{ $quote }}</h2>

            <div class="border-l-2 border-l-solid border-gray-300 pl-8">
                {{ html()->formGroup()->required()->text('quote_author_'.$quote, 'Author')->class('sm:w-1/2 lg:w-1/3') }}

                <p class="text-sm -mt-4 mb-8 text-gray-500">Leave empty to hide the quote.</p>

                {{ html()->translations(function () use ($quote) {
                    return [
                        html()->formGroup()->required()->text('quote_text_'.$quote, 'Text'),
                    ];
                }) }}

                {{ html()->formGroup()->media('quote_image_'.$quote, 'image', 'Author portrait') }}
            </div>
        @endforeach
    </div>
    <div id="blocks">
        {{ html()->formGroup()->checkbox('show_content_blocks_toc', 'Show table of contents for content blocks') }}

        {{ html()->formGroup()->contentBlocks('default', 'Blocks', [
            'types' => [
                'imageLeft' => 'Block with an image to the left',
                'imageRight' => 'Block with an image to the right',
                'contactRight' => 'Block with a contact form to the right',
            ],
            'translatableAttributes' => [
                'name' => 'text',
                'text' => 'redactor',
            ],
            'mediaLibraryCollections' => [
                'images' => 'images',
            ],
            'labels' => [
                'images' => 'Image(s)',
            ],
        ]) }}
    </div>
    <div id="settings">
        @if(! $model->isSpecialArticle())
            {{ html()->formGroup()->checkbox('online', 'Online') }}
        @endif

        {{ html()->formGroup()->searchableSelect('parent_id', 'Parent article', $parentMenuItems) }}
    </div>
    <div id="seo">
        {{ html()->seo() }}

        <div class="alert--info">
            We automatically determine these fields' contents. You can override them here if you need
            something special.
        </div>
    </div>
</div>

@if ($fragment->image)
    <div class="form__group">
        {{ html()->formGroup()->media('images', 'image', 'Image') }}
    </div>
@else
    @foreach(locales() as $locale)
        @php(html()->locale($locale))

        <div class="form__group">
            {{ html()->label(html()->span($locale)->class('label--lang'), 'text') }}
            {{ html()
                ->{$fragment->html ? 'redactor' : 'text'}('text')
                ->value(old(translate_field_name('text'), $fragment->getTranslation($locale)))
            }}
            {{ html()->errorFor('text') }}

            {{ html()->error($errors->first(translate_field_name('text', $locale))) }}
        </div>

        @php(html()->endLocale())
    @endforeach
@endif

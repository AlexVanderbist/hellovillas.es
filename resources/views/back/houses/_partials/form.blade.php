<div data-tabs class="tabs">
    <nav class="tabs__menu">
        <ul>
            <li><a href="#content" class="js-tabs-nav"><i class="fa fa-edit"></i> Content</a></li>
            <li><a href="#location" class="js-tabs-nav"><i class="fa fa-location-arrow"></i> Location</a></li>
            <li><a href="#amenities" class="js-tabs-nav"><i class="fa fa-check-square-o"></i> Amenities</a></li>
            <li><a href="#rental" class="js-tabs-nav"><i class="fa fa-airbnb"></i> Rental</a></li>
            <li><a href="#sale" class="js-tabs-nav"><i class="fa fa-pen-nib"></i> Sale</a></li>
            <li><a href="#settings" class="js-tabs-nav"><i class="fa fa-cog"></i> Settings</a></li>
            <li><a href="#seo" class="js-tabs-nav"><i class="fa fa-code"></i> SEO</a></li>
        </ul>
    </nav>
    <div id="content">
        {{ html()->translations(function () {
            return [
                html()->formGroup()->required()->text('name', 'Name'),
                html()->formGroup()->required()->redactor('text', 'Text'),
            ];
        }) }}

        {{ html()->formGroup()->media('images', 'images', 'Images') }}
        {{ html()->formGroup()->media('banner', 'image', 'Banner') }}
    </div>
    <div id="amenities">
        <div class="grid no-padding">
            {{ html()->formGroup()->required()->number('bedrooms', 'Number of bedrooms')->class('-width-1/2 grid__col') }}
            {{ html()->formGroup()->required()->number('bathrooms', 'Number of bathrooms')->class('-width-1/2 grid__col') }}
            {{ html()->formGroup()->required()->number('double_beds', 'Number of double beds')->class('-width-1/2 grid__col') }}
            {{ html()->formGroup()->required()->number('single_beds', 'Number of single beds')->class('-width-1/2 grid__col') }}
        </div>

        <div class="grid no-padding">
            <div class="grid__col -width-1/2">
                {{ html()->formGroup()->checkboxArray('amenities', 'Pool', false, 'pool') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'TV', false, 'tv') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'A/C', false, 'ac') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'Wi-Fi', false, 'wifi') }}
            </div>
            <div class="grid__col -width-1/2">
                {{ html()->formGroup()->checkboxArray('amenities', 'Bath', false, 'bath') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'Pet friedly', false, 'pets') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'Car parking', false, 'parking') }}
                {{ html()->formGroup()->checkboxArray('amenities', 'Outside kitchen', false, 'kitchen') }}
            </div>
        </div>
    </div>
    <div id="location">
        {{ html()->formGroup()->map('location', 'Location') }}

        {{ html()->translations(function () {
            return [
                html()->formGroup()->required()->text('location_name', 'Location name'),
                html()->formGroup()->required()->redactor('location_text', 'Location text'),
            ];
        }) }}
    </div>
    <div id="rental">
        {{ html()->formGroup()->checkbox('for_rent', 'This is a rental house')->class('enable-checkbox') }}

        <hr>

        <div class="enable-zone">
            {{ html()->formGroup()->checkbox('is_available', 'House is available for rent') }}

            <div class="grid no-padding">
                {{ html()->translations(function () {
                    return [
                        html()->formGroup()->required()->text('check_in', 'Check-in')->class('-width-1/2 grid__col'),
                        html()->formGroup()->required()->text('check_out', 'Check-out')->class('-width-1/2 grid__col'),
                    ];
                }) }}
            </div>

            {{ html()->formGroup()->text('booking_url', 'External booking URL (AirBNB?)') }}
        </div>
    </div>
    <div id="sale">
        {{ html()->formGroup()->checkbox('for_sale', 'This house is for sale')->class('enable-checkbox') }}

        <hr>

        {{ html()->formGroup()->required()->number('price', 'Price (&euro;)') }}

        <div class="enable-zone">
            <div class="grid no-padding">
                {{ html()->formGroup()->required()->number('interior_area', 'Interior area (&#13217)')->class('-width-1/2 grid__col') }}
                {{ html()->formGroup()->required()->number('exterior_area', 'Exterior area (&#13217)')->class('-width-1/2 grid__col') }}
            </div>
        </div>
    </div>
    <div id="settings">
        {{ html()->formGroup()->checkbox('online', 'Online') }}

        {{ html()->formGroup()->searchableSelect('article_id', 'Menu section', $topLevelArticles) }}
    </div>
    <div id="seo">
        {{ html()->seo() }}

        <div class="alert--info">
            We automatically determine these fields' contents. You can override them here if you need something special.
        </div>
    </div>
</div>

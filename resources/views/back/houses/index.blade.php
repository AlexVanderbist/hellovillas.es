@component('back._layouts.master', [
    'title' => 'Houses',
])
    <section>
        <div class="grid">
            <h1>Houses</h1>

            <a href="{{ action('Back\HousesController@create') }}" class="button">
                New house
            </a>

            <table data-sortable="{{ action('Back\HousesController@changeOrder') }}">
                <thead>
                <tr>
                    <th>Name</th>
                    <th data-orderable="false"></th>
                    <th data-orderable="false"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($models as $house)
                    <tr data-row-id="{{ $house->id }}">
                        <td>
                            {{ html()->onlineIndicator($house->online) }}
                            <a href="{{ action('Back\HousesController@edit', [$house->id]) }}">
                                {{ $house->name }}
                            </a>
                        </td>
                        <td class="-remark">
                            @if($house->for_rent)
                                For rent
                            @endif
                            @if($house->for_rent && $house->for_sale)
                                /
                            @endif
                            @if($house->for_sale)
                                For sale
                            @endif
                        </td>
                        <td class="-right">
                            {{ html()->deleteButton(action('Back\HousesController@destroy', $house->id)) }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </section>
@endcomponent

<?php

use App\Models\Enums\SpecialArticle;
use App\Models\House;

class HouseSeeder extends DatabaseSeeder
{
    public function run()
    {
        $this->truncate((new House())->getTable());

        collect([
            ['Casa Alegria', 'Jalon'],
            ['Casa Muneca', 'Jalon'],
            ['Casa Summer', 'Denia'],
            ['Finca Benidorm', 'Benidorm'],
            ['Casita Benisa', 'Benisa'],
        ])->each(function ($attributes) {
            $this->seedHouse(...$attributes);
        });
    }

    public function seedHouse(?string $name = null, ?string $location = null): House
    {
        $house = House::create([
            'name' => faker()->translate($name) ?: faker()->translate(faker()->title()),
            'text' => faker()->translate(faker()->text()),
            'location_name' => $location ?: faker()->translate(faker()->city()),
            'location_text' => faker()->translate(faker()->text()),
            'location_lat' => faker()->randomFloat(2, 1, 10),
            'location_lng' => faker()->randomFloat(2, 1, 10),
            'location_zoom' => faker()->numberBetween(1, 5),
            'check_in' => faker()->translate('after 15:00'),
            'check_out' => faker()->translate('before 10:00'),
            'article_id' => article(SpecialArticle::PROPERTIES_TO_RENT)->id,
            'meta_values' => collect([]),
            'online' => true,
            'draft' => false,
        ]);

        $this->addImages($house, 3, 4, 'images');
        $this->addImages($house, 1, 1, 'banner', 'landscapes');

        return $house;
    }
}

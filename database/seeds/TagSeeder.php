<?php

use App\Models\Tag;
use Illuminate\Support\Collection;

class TagSeeder extends DatabaseSeeder
{
    public function run()
    {
        $this->truncate((new Tag())->getTable(), 'taggables');

        $this->createTags('amenities', [
            'Pool',
            'Wi-Fi',
            'Shower',
        ]);
    }

    public function createTags(string $type, array $names): Collection
    {
        return collect($names)->each(function ($name) use ($type) {
            $this->createTag([
                'name' => faker()->translate($name),
                'type' => $type,
            ]);
        });
    }

    public function createTag(array $attributes = []): Tag
    {
        return Tag::create($attributes + [
            'type' => 'default',
            'name' => faker()->translate(faker()->name()),
            'draft' => 0,
            'online' => 1,
        ]);
    }
}

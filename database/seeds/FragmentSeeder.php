<?php

use App\Models\Fragment;

class FragmentSeeder extends DatabaseSeeder
{
    public function run()
    {
        $this->truncate((new Fragment())->getTable());

        Artisan::call('fragments:import', ['--update' => true]);

        Fragment::whereIn('key', [
            'footerImage',
            'quoteImage',
        ])->update(['image' => true]);

        Fragment::where('image', true)->get()->each(function (Fragment $fragment) {
            $this->addImages($fragment, 1, 1);
        });
    }
}

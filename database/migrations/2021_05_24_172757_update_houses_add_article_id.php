<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHousesAddArticleId extends Migration
{
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->unsignedInteger('article_id')->nullable();
            $table->foreign('article_id')->references('id')->on('articles');
        });
    }
}

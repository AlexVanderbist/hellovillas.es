<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHousesAddAvailability extends Migration
{
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->boolean('is_available')->default(true);
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHousesAddForSaleAndForRent extends Migration
{
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->boolean('for_sale')->default(false);
            $table->boolean('for_rent')->default(true);
            $table->unsignedInteger('interior_area')->nullable();
            $table->unsignedInteger('exterior_area')->nullable();
            $table->unsignedInteger('price')->nullable();
        });
    }
}

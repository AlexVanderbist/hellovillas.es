<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArticlesAddQuotes extends Migration
{
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->json('quote_text_1')->nullable();
            $table->string('quote_author_1')->nullable();

            $table->json('quote_text_2')->nullable();
            $table->string('quote_author_2')->nullable();

            $table->json('quote_text_3')->nullable();
            $table->string('quote_author_3')->nullable();
        });
    }
}

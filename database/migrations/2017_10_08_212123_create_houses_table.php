<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->increments('id');
            $table->json('name')->nullable();
            $table->json('text')->nullable();
            $table->text('booking_url')->nullable();

            $table->json('location_name')->nullable();
            $table->json('location_text')->nullable();
            $table->decimal('location_lat', 18, 14)->nullable();
            $table->decimal('location_lng', 18, 14)->nullable();
            $table->integer('location_zoom')->default(3);

            $table->json('amenities')->nullable();

            $table->integer('bathrooms')->nullable()->default(0);
            $table->integer('bedrooms')->nullable()->default(0);
            $table->integer('single_beds')->nullable()->default(0);
            $table->integer('double_beds')->nullable()->default(0);

            $table->json('check_in')->nullable();
            $table->json('check_out')->nullable();

            $table->json('slug')->nullable();
            $table->json('meta_values')->nullable();

            $table->integer('end_cleaning_price')->default(60);
            $table->integer('mid_cleaning_price')->default(30);

            $table->integer('order_column')->nullable();
            $table->boolean('draft')->default(true);
            $table->boolean('online')->default(true);
            $table->timestamps();
        });
    }
}

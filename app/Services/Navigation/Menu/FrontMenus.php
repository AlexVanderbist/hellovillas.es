<?php

namespace App\Services\Navigation\Menu;

use app\Models\Article;
use App\Models\Enums\SpecialArticle;
use App\Repositories\ArticleRepository;
use Spatie\Menu\Laravel\Link;
use Spatie\Menu\Laravel\Menu;
use Spatie\Menu\Laravel\View;

class FrontMenus
{
    public function register()
    {
        Menu::macro('front', function () {
            return Menu::new()->setActiveFromRequest('/' . locale());
        });

        Menu::macro('main', function () {
            $menu = Menu::front()
                ->addClass('navbar-end')
                ->setActiveClass('active')
                ->article(SpecialArticle::HOME, 'Home');

            ArticleRepository::getTopLevel()
                ->filter(function (Article $article) {
                    return $article->technical_name != SpecialArticle::HOME;
                })
                ->each(function (Article $article) use ($menu) {
                    if ($article->hasChildren() || $article->hasHouses()) {
                        $menu
                        ->add(View::create('front._layouts._partials.dropdownMenu', ['article' => $article])
                        ->addParentClass('navbar-item has-dropdown is-hoverable'));
                    } else {
                        $menu->article($article);
                    }
                });

            return $menu
                ->each(function (Link $link) {
                    $link->addParentClass('navbar-item');
                })
                ->add(View::create('front._layouts._partials.languageMenu')
                    ->addParentClass('navbar-item has-dropdown is-hoverable'));
        });

        Menu::macro('articleSiblings', function (Article $article) {
            return $article->siblings->reduce(function (Menu $menu, Article $article) {
                return $menu->url($article->url, $article->name);
            }, Menu::front());
        });

        Menu::macro('article', function ($article, ?string $name = null) {
            $article = $article instanceof Article ? $article : article($article);

            return $this->url($article->url, $name ?? $article->name);
        });
    }
}

<?php

namespace App\Services\Price;

use App\Models\House;
use Carbon\Carbon;

class Price
{
    protected $checkIn;

    protected $checkOut;

    protected $house;

    public function __construct(House $house, Carbon $checkIn, Carbon $checkOut)
    {
        $this->checkIn = $checkIn;
        $this->checkOut = $checkOut;
        $this->house = $house;
    }

    public function getBasePrice(): int
    {
        $price = 0;

        for ($day = $this->checkIn->copy(); $day->lt($this->checkOut); $day->addDay()) {
            $price += $this->getPricePerNight($day);
        }

        return $price;
    }

    public function getEndCleaningPrice(): int
    {
        return (int) $this->house->end_cleaning_price;
    }

    public function getMidCleaningPrice(): int
    {
        return floor(($this->checkIn->diffInDays($this->checkOut) - 1) / 7) * (int) $this->house->mid_cleaning_price;
    }

    public function getTotalPrice(): int
    {
        return $this->getBasePrice() + $this->getEndCleaningPrice() + $this->getMidCleaningPrice();
    }

    public function getPriceArray(): array
    {
        return [
            'total' => $this->getTotalPrice(),
            'mid_cleaning_total' => $this->getMidCleaningPrice(),
            'end_cleaning_total' => $this->getEndCleaningPrice(),
            'mid_cleaning' => $this->house->mid_cleaning_price,
            'sub_total' => $this->getBasePrice(),
        ];
    }

    protected function getPricePerNight(Carbon $day)
    {
        return $this->house->getPeriod($day->weekOfYear)->price_per_night;
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Cache;

class Media extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        list($width, $height) = rescue(function () {
            return getimagesize($this->getPath('2400w'));
        }, function () {
            return [2400, 1400];
        });

        $conversionUrls = collect(['2400w', '1800w', '1200w', '600w'])
            ->mapWithKeys(function ($conversion) {
                return ["{$conversion}Url" => $this->getUrl($conversion)];
            })->toArray();

        return $conversionUrls + [
            'id' => $this->id,
            'name' => $this->name,
            'collection' => $this->collection_name,
            'fileName' => $this->file_name,
            'customProperties' => $this->custom_properties,
            'orderColumn' => $this->order_column,
            'height' => $height,
            'width' => $width,
            'thumbUrl' => strtolower($this->extension) === 'svg' ?
                $this->getUrl() :
                $this->getUrl('thumb'),
            'placeholderBase64' => $this->getPlaceholderBase64Attribute(),
            'originalUrl' => $this->getUrl(),
        ];
    }

    public function getPlaceholderBase64Attribute(): string
    {
        if (! file_exists($this->getPath('placeholder'))) {
            return '';
        }

        $minutes = 60 * 24;
        $cacheKey = "media.{$this->id}.placeholder";

        return Cache::remember($cacheKey, $minutes, function () {
            return base64_encode(
                file_get_contents($this->getPath('placeholder'))
            );
        });
    }
}

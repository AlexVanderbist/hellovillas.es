<?php

namespace App\Http\ViewComposers\Front;

use App\Repositories\HouseRepository;
use Illuminate\Contracts\View\View;

class HousesMenuViewComposer
{
    public function compose(View $view)
    {
        $houses = HouseRepository::getAll();

        $view->with(compact('houses'));
    }
}

<?php

namespace App\Http\ViewComposers\Front;

use App\Models\Enums\SpecialArticle;
use App\Models\Fragment;
use App\Repositories\HouseRepository;
use Illuminate\Contracts\View\View;

class FooterViewComposer
{
    public function compose(View $view)
    {
        $backgroundImage = Fragment::where('key', 'footerImage')->first()
            ->getFirstMedia('images');

        $houses = HouseRepository::propertiesToRent();

        $services = article(SpecialArticle::SERVICE)->children;

        $view->with(compact('backgroundImage', 'houses', 'services'));
    }
}

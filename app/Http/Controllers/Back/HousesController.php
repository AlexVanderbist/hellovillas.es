<?php

namespace App\Http\Controllers\Back;

use App\Models\Article;
use App\Models\Enums\SpecialArticle;
use App\Models\House;
use Illuminate\Http\Request;

class HousesController extends Controller
{
    protected $modelName = 'Article';

    protected function make(): House
    {
        return House::create();
    }

    public function edit(int $id)
    {
        $topLevelArticles = Article::query()
            ->where('parent_id', null)
            ->orderBy('order_column')
            ->get()
            ->filter(function (Article $article) {
                return $article->technical_name != SpecialArticle::HOME;
            })
            ->pluck('name', 'id');

        return parent::edit($id)
            ->with(compact('topLevelArticles'));
    }

    protected function updateFromRequest(House $model, Request $request)
    {
        $model->amenities = $request->get('amenities');

        $this->updateFields($model, $request, [
            'for_rent', 'for_sale', 'interior_area', 'exterior_area', 'price',
            'article_id','booking_url', 'location_lat', 'location_lng', 'location_zoom',
            'bathrooms', 'bedrooms', 'single_beds', 'double_beds', 'is_available',
        ]);

        $this->updateModel($model, $request);
    }

    protected function validationRules(): array
    {
        $rules = [
            'publish_date' => 'date_format:d/m/Y',
        ];

        foreach (config('app.locales') as $locale) {
            $rules[translate_field_name('name', $locale)] = 'required';
            $rules[translate_field_name('text', $locale)] = 'required';
        }

        return $rules;
    }
}

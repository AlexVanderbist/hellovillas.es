<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Enums\SpecialArticle;
use Illuminate\Database\Eloquent\Builder;

class HomeController extends Controller
{
    public function index()
    {
        $article = article(SpecialArticle::HOME);

        $houseArticles = Article::query()
            ->online()
            ->with('houses')
            ->whereHas('houses', function (Builder $query) {
                $query->online();
            })
            ->get()
            ->mapWithKeys(function (Article $article) {
                return [$article->name => $article->houses()->online()->get()];
            });

        return view('front.home.index', compact('article', 'houseArticles'));
    }
}

<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\House;
use App\Repositories\HouseRepository;

class HousesController extends Controller
{
    public function show(?string $houseSlug = null)
    {
        if (! $houseSlug) {
            return redirect()->route('houses.show', House::first()->slug);
        }

        $house = HouseRepository::findBySlug($houseSlug);

        return view('front.houses.show', compact('house'));
    }
}

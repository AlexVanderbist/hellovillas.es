<?php

namespace App\Http\Controllers\Front\Api;

use App\Models\House;
use App\Services\Price\Price;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function __invoke(Request $request, $houseId)
    {
        $house = House::findOrFail($houseId);

        $dates = $request->validate([
            'check_in' => 'required',
            'check_out' => 'required',
        ]);

        $checkIn = new Carbon($dates['check_in']);
        $checkOut = new Carbon($dates['check_out']);

        $priceCalculator = new Price($house, $checkIn, $checkOut);

        return $priceCalculator->getPriceArray();
    }
}

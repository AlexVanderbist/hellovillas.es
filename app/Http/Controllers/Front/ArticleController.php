<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Enums\SpecialArticle;
use App\Repositories\ArticleRepository;
use App\Repositories\HouseRepository;

class ArticleController extends Controller
{
    public function index(...$articleSlugs)
    {
        $articleSlug = collect($articleSlugs)->last();

        $article = ArticleRepository::findBySlug($articleSlug);

        if ($article->hasChildren()) {
            return redirect($article->first_child->url);
        }

        if ($article->hasHouses()) {
            return redirect($article->first_house->url);
        }

        $houses = $article->isSpecialArticle(SpecialArticle::PROPERTIES_FOR_SALE)
            ? HouseRepository::propertiesForSale()
            : null;

        return view('front.article.index', compact('article', 'houses'));
    }
}

<?php

namespace App\Models;

use App\Models\Enums\SpecialArticle;
use App\Models\Presenters\ArticlePresenter;
use App\Models\Traits\HasContentBlocks;
use App\Models\Traits\HasSlug;
use Exception;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\MediaLibrary\Media;

/**
 * @property \App\Models\Article $parent
 * @property \Illuminate\Support\Collection $children
 * @property \App\Models\Article $firstChild
 * @property \Illuminate\Support\Collection $siblings
 */
class Article extends Model implements Sortable
{
    use ArticlePresenter;
    use HasSlug;
    use SortableTrait;
    use HasContentBlocks;

    protected $with = ['media'];

    protected $mediaLibraryCollections = ['images', 'downloads', 'quote_image_1', 'quote_image_2', 'quote_image_3'];

    protected $contentBlockMediaLibraryCollections = ['images'];

    public $translatable = [
        'name',
        'text',
        'slug',
        'meta_values',
        'quote_text_1',
        'quote_text_2',
        'quote_text_3',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        parent::registerMediaConversions();

        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->optimize()
            ->performOnCollections('images');

        $this->addMediaConversion('thumb_square')
            ->width(368)
            ->height(368)
            ->crop('crop-center', 368, 368)
            ->optimize()
            ->performOnCollections('quote_image_1', 'quote_image_2', 'quote_image_3');
    }

    public function isSpecialArticle($specialArticleName = ''): bool
    {
        if ($specialArticleName === '') {
            return ! empty($this->technical_name);
        }

        return $this->technical_name === $specialArticleName;
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order_column');
    }

    public function hasChildren(): bool
    {
        return $this->children()->count();
    }

    public function houses(): HasMany
    {
        return $this->hasMany(House::class, 'article_id')->orderBy('order_column');
    }

    public function hasHouses(): bool
    {
        return $this->houses()->count();
    }

    public function getFirstChildAttribute(): Article
    {
        if (! $this->hasChildren()) {
            throw new Exception("Article `{$this->id}` doesn't have any children.");
        }

        return $this->children->sortBy('order_column')->first();
    }

    public function getFirstHouseAttribute(): House
    {
        if (! $this->hasHouses()) {
            throw new Exception("Article `{$this->id}` doesn't have any houses.");
        }

        return $this->houses->sortBy('order_column')->first();
    }

    public function getSiblingsAttribute(): Collection
    {
        return self::where('parent_id', $this->parent_id)
            ->orderBy('order_column')
            ->get();
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function hasParent(): bool
    {
        return ! is_null($this->parent);
    }

    public function getUrlAttribute(): string
    {
        $localeSegment = '';

        if (locales()->count() > 1) {
            $localeSegment = '/'.locale();
        }

        if ($this->technical_name === SpecialArticle::HOME) {
            return $localeSegment;
        }

        $parentSlug = $this->hasParent() ? $this->parent->slug.'/' : '';

        return url("{$localeSegment}/{$parentSlug}{$this->slug}");
    }

    public function getQuote(int $number = 1): array
    {
        return [
            'number' => $number,
            'text' => $this->{'quote_text_'.$number},
            'author' => $this->{'quote_author_'.$number},
            'image' => $this->getMedia("quote_image_{$number}")->first(),
        ];
    }
}

<?php

namespace App\Models\Presenters;

trait HousePresenter
{
    public function getExcerptAttribute(): string
    {
        return str_tease($this->text);
    }

    public function getGoogleMapsUrlAttribute(): string
    {
        return "https://www.google.com/maps/search/?api=1&query={$this->location_lat},{$this->location_lng}&zoom={$this->location_zoom}";
    }
}

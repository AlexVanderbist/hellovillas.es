<?php

namespace App\Models;

use App\Http\Resources\Media as MediaResource;
use App\Models\Presenters\HousePresenter;
use App\Models\Traits\HasSlug;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class House extends Model implements Sortable
{
    use HasSlug;
    use HousePresenter;
    use SortableTrait;

    protected $with = ['media'];

    public $casts = [
        'amenities' => 'array',
        'is_available' => 'boolean',
    ];

    public $translatable = ['name', 'text', 'location_name', 'location_text', 'check_in', 'check_out', 'slug', 'meta_values'];

    protected $mediaLibraryCollections = ['images', 'banner'];

    public function getMediaResource(string $collection = 'images'): array
    {
        return MediaResource::collection($this->getMedia($collection))->resolve();
    }

    public function getUrlAttribute(): string
    {
        $localeSegment = '';

        if (locales()->count() > 1) {
            $localeSegment = '/'.locale();
        }

        $parentSlug = 'houses';

        return url("{$localeSegment}/{$parentSlug}/{$this->slug}");
    }

    public function hasExternalBooking(): bool
    {
        return ! is_null($this->booking_url);
    }

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }
}

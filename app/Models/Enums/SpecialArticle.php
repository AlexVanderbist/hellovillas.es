<?php

namespace App\Models\Enums;

class SpecialArticle
{
    const HOME = 'home';

    const PROPERTIES_TO_RENT = 'properties-to-rent';

    const PROPERTIES_FOR_SALE = 'properties-for-sale';

    const SERVICE = 'service';

    const CONTACT = 'contact';
}

<?php

namespace App\Repositories;

use App\Models\Enums\SpecialArticle;
use App\Models\House;
use Illuminate\Support\Collection;

class HouseRepository
{
    public static function getAll(): Collection
    {
        return House::get()->reverse();
    }

    public static function propertiesToRent(): Collection
    {
        return article(SpecialArticle::PROPERTIES_TO_RENT)->houses()->get();
    }

    public static function propertiesForSale(): Collection
    {
        return House::where('for_sale', true)->get()->reverse();
    }

    public static function findById(int $id): House
    {
        return House::findOrFail($id);
    }

    public static function findBySlug(string $slug): House
    {
        return House::where('slug->'.content_locale(), $slug)->firstOrFail();
    }
}
